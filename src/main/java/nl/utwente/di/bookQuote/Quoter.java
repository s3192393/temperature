package nl.utwente.di.bookQuote;

public class Quoter {

    public double getFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 1.8) + 32;
    }
}
